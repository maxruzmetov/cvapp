// Core
import { loginActions } from '../../bus/login/actions';
import { useSelector, useDispatch } from 'react-redux';

export const useNavigation = () => {
  const dispatch = useDispatch();

  const { loggedIn } = useSelector((state) => state.login);

  const logoutHandler = () => {
    dispatch(loginActions.setLoginStatus(false));
  };

  return { loggedIn, logoutHandler };
};
