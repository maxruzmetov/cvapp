//Core
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { book } from './book';

//Components
import { Header } from '../components/header';
import { Registration } from '../pages/registration';
import { Login } from '../pages/login';
import { Profile } from '../pages/profile';

//Hooks
import { useNavigation } from './hooks/useNavigation';

export const Routes = () => {
  const { loggedIn } = useNavigation();

  return (
    <>
      <Header />
      <Switch>
        <Route exact path={book.root}>
          {loggedIn ? <Redirect to={book.profile} /> : <Registration />}
        </Route>
        <Route exact path={book.login}>
          {loggedIn ? <Redirect to={book.profile} /> : <Login />}
        </Route>
        <Route exact path={book.profile}>
          {loggedIn ? <Profile /> : <Redirect to={book.login} />}
        </Route>
      </Switch>
    </>
  );
};
