import React from 'react';

import { Registration as RegistrationComponent } from '../../bus/registration';

export const Registration = () => {
  return <RegistrationComponent />;
};
