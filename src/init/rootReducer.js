// Core
import { combineReducers } from 'redux';

// Reducers

import { loginReducer as login } from '../bus/login/reducer';
import { profileReducer as profile } from '../bus/profile/reducer';

export const rootReducer = combineReducers({
  login,
  profile,
});
