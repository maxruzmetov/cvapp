import { types } from './types';

export const loginActions = {
  setLoginStatus: (status) => {
    return {
      type: types.SET_LOGIN_STATUS,
      payload: status,
    };
  },
};
