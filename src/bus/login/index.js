//Core
import React, { useCallback } from 'react';
import { Formik, Form } from 'formik';

//Styles
import './styles.scss';

//Elements
import { MyTextInput } from '../../elements/inputs/myTextInput';

//Hooks
import { useLogin } from './hooks/useLogin';
import { useLoginFormManagement } from './hooks/useLoginFormManagement';

export const Login = () => {
  const { sendForm, isFormBlocked, timeLeft } = useLogin();

  const { loginData, validationSchema } = useLoginFormManagement();

  const onFormSubmit = useCallback(
    (values) => {
      sendForm(values);
    },
    [sendForm]
  );

  const stylesCX = {
    color: '#800000',
    fontSize: '18',
    fontWeight: '600',
  };

  const errorMessageJSX = isFormBlocked && (
    <p style={stylesCX}>Form blocked for {timeLeft} sec</p>
  );

  return (
    <section>
      <h1>Login</h1>
      {errorMessageJSX}
      <Formik
        initialValues={loginData}
        validationSchema={validationSchema}
        onSubmit={onFormSubmit}
      >
        <Form>
          <MyTextInput
            type="text"
            name="login"
            id="login"
            label="Login"
            placeholder="Enter your login"
          />

          <MyTextInput
            type="password"
            name="password"
            id="surName"
            label="Password"
            placeholder="Enter your password"
            autoComplete="off"
          />

          <button type="submit" disabled={isFormBlocked}>
            Submit
          </button>
        </Form>
      </Formik>
    </section>
  );
};
