import { types } from './types';

const initialState = {
  loggedIn: false,
};

export const loginReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_LOGIN_STATUS:
      return {
        ...state,
        loggedIn: payload,
      };
    default:
      return state;
  }
};
