//Core
import * as Yup from 'yup';

export const useLoginFormManagement = () => {
  const loginData = {
    login: '',
    password: '',
  };

  const validationSchema = Yup.object({
    login: Yup.string().required('Required'),
    password: Yup.string().required('Required'),
  });

  return {
    loginData,
    validationSchema,
  };
};
