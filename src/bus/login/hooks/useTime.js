import { useState } from 'react';

export const useTime = () => {
  const getCurrentTime = () => {
    return Math.round(new Date().getTime() / 1000);
  };

  const getBlockedTime = () => {
    return localStorage.getItem('blockerTimer');
  };

  const timeDifference = (cur, storage) => {
    if (storage !== null && cur < storage) {
      return storage - cur;
    }

    return 0;
  };

  const getTimerValue = () => {
    return timeDifference(getCurrentTime(), getBlockedTime());
  };

  const onIntervalEvent = (interval) => {
    setTimeLeft(getTimerValue());

    if (getTimerValue() === 0) {
      localStorage.removeItem('blockerTimer');
      clearInterval(interval);
    }
  };

  const [timeLeft, setTimeLeft] = useState(getTimerValue());

  return {
    onIntervalEvent,
    timeLeft,
    setTimeLeft,
  };
};
