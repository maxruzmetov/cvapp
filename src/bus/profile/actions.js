import { types } from './types';

export const profileActions = {
  getProfile: (profile) => {
    return {
      type: types.GET_PROFILE,
      payload: profile,
    };
  },
  addSkill: (profile) => {
    return {
      type: types.GET_PROFILE,
      payload: profile,
    };
  },
};
