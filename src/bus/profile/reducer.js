import { types } from './types';

const initialState = {
  skillsList: [
    {
      id: 0,
      title: 'First Skill',
      description: 'this is demo text for example',
    },
    {
      id: 2,
      title: 'Second Skill',
      description: 'this is demo text for example',
    },
    {
      id: 3,
      title: 'Second Skill',
      description: 'this is demo text for example',
    },
  ],
};

export const profileReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SAVE_PROFILE:
      return {
        data: payload,
      };
    default:
      return state;
  }
};

// import { types } from './types';

// const initialState = {
//   weatherData: [],
//   isFetching: false,
//   error: false,
//   selectedDay: null,
//   isFiltered: false,
//   locale: 'ru',
// };

// export const weatherReducer = (state = initialState, { type, payload }) => {
//   switch (type) {
//     case types.WEATHER_START_FETCHING:
//       return {
//         ...state,
//         isFetching: true,
//       };
//     case types.WEATHER_FILL:
//       return {
//         ...state,
//         weatherData: payload,
//         isFetching: false,
//         isFiltered: false,
//       };
//     case types.WEATHER_SET_DAY:
//       return {
//         ...state,
//         selectedDay: payload,
//       };
//     case types.WEATHER_FILTER_DAYS:
//       const filter = payload;

//       const filteredWeatherData = state.weatherData.filter((day) => {
//         const isType = filter.type ? filter.type === day.type : true;
//         const isMinTemp = filter.minTemp
//           ? filter.minTemp <= day.temperature
//           : true;
//         const isMaxTemp = filter.maxTemp
//           ? filter.maxTemp >= day.temperature
//           : true;

//         return isType && isMinTemp && isMaxTemp;
//       });

//       return {
//         ...state,
//         weatherData: filteredWeatherData,
//         isFiltered: true,
//       };
//     case types.WEATHER_SET_FETCHING_ERROR:
//       return {
//         ...state,
//         error: payload,
//         isFetching: false,
//       };

//     default:
//       return state;
//   }
// };
