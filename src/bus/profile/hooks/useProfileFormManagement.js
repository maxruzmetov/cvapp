import * as Yup from 'yup';

export const useProfileFormManagement = () => {
  const initialValues = {
    title: '',
    description: '',
  };

  const validationSchema = Yup.object({
    title: Yup.string().required('Required'),
    description: Yup.string().required('Required'),
  });

  return {
    initialValues,
    validationSchema,
  };
};
