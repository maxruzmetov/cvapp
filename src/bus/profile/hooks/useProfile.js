//Core
// import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

//Actions
import { profileActions } from '../actions';

export const useProfile = () => {
  const dispatch = useDispatch();

  const { skillsList } = useSelector((state) => state.profile);

  const addSkill = (skill) => {
    dispatch(profileActions.addSkill(skill));
  };

  return {
    skillsList,
    addSkill,
  };
};
