// Core
import React from 'react';
// import { Formik, Form } from 'formik';

// Styles
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

//Hooks
import { useProfile } from './hooks/useProfile';
// import { useProfileFormManagement } from './hooks/useProfileFormManagement';

//Elements
// import { MyTextInput } from '../../elements/inputs/myTextInput';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
    maxWidth: 500,
    backgroundColor: '#fff',
  },
  item: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#e0e0e0',
    marginBottom: 10,
  },
  delete: {
    margin: '0 0 0 10px',
    padding: '5px 10px;',
  },
}));

export const Profile = () => {
  let { skillsList } = useProfile();

  const classes = useStyles();

  const skillsListJSX = skillsList.map(({ id, title, description }) => (
    <ListItem button key={id} className={classes.item}>
      <ListItemText primary={title} />
      <ListItemText primary={description} />
      <button className={classes.delete}>X</button>
    </ListItem>
  ));

  // const formJSX = (
  //   <Formik
  //     initialValues={initialValues}
  //     validationSchema={validationSchema}
  //     onSubmit={onFormSubmit}
  //   >
  //     <Form>
  //       <MyTextInput
  //         type="text"
  //         name="login"
  //         id="login"
  //         label="Login"
  //         placeholder="Jane"
  //       />

  //       <MyTextInput
  //         type="password"
  //         name="password"
  //         id="surName"
  //         label="Password"
  //         placeholder="Doe"
  //         autoComplete="on"
  //       />

  //       <button type="submit">Add</button>
  //     </Form>
  //   </Formik>
  // );

  return (
    <section>
      <h1>Profile</h1>
      <p>
        <button>Add more</button>
      </p>
      {/* {formJSX} */}
      <List
        component="nav"
        className={classes.root}
        aria-label="mailbox folders"
      >
        {skillsListJSX}
      </List>
    </section>
  );
};
