import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';

import { book } from '../../../navigation/book';

export const useRegFormManagement = () => {
  let regRedirect = useHistory();

  const initialValues = {
    login: '',
    password: '',
  };

  const validationSchema = Yup.object({
    login: Yup.string()
      .max(15, 'Must be 15 characters or less')
      .required('Required'),
    password: Yup.string()
      .required('Required')
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
      ),
  });

  const handleSubmit = (values) => {
    localStorage.setItem('loginData', JSON.stringify(values));
    alert('Registration Success');
    regRedirect.push(book.login);
  };

  return {
    initialValues,
    validationSchema,
    handleSubmit,
  };
};
