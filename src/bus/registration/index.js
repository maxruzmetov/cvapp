//Core
import React, { useCallback } from 'react';
import { Formik, Form } from 'formik';

//Styles
import './styles.scss';

//Elements
import { MyTextInput } from '../../elements/inputs/myTextInput';

//Hooks
import { useRegFormManagement } from './hooks/useRegFormManagement';

export const Registration = () => {
  const {
    initialValues,
    validationSchema,
    handleSubmit,
  } = useRegFormManagement();

  const onFormSubmit = useCallback(
    (values) => {
      handleSubmit(values);
    },
    [handleSubmit]
  );

  return (
    <section>
      <h1>Registration</h1>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onFormSubmit}
      >
        <Form>
          <MyTextInput
            type="text"
            name="login"
            id="login"
            label="Login"
            placeholder="Jane"
          />

          <MyTextInput
            type="password"
            name="password"
            id="surName"
            label="Password"
            placeholder="psw.exmp Qq1!qqqq"
          />

          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </section>
  );
};
