//Core
import React from 'react';
import Styles from './style.module.scss';
import { Link } from 'react-router-dom';
import { book } from '../../navigation/book';

//Hooks
import { useNavigation } from '../../navigation/hooks/useNavigation';

export const Header = () => {
  const { loggedIn, logoutHandler } = useNavigation();

  const logoutLinkJSX = loggedIn && (
    <Link to={book.login} onClick={logoutHandler}>
      Logout
    </Link>
  );

  return (
    <section className={Styles.header}>
      <nav>
        <Link to={book.root}>Registration</Link>
        <Link to={book.login}>Login</Link>
        <Link to={book.profile}>Profile</Link>
        {logoutLinkJSX}
      </nav>
    </section>
  );
};
